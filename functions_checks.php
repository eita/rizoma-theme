<?php

function rizoma_add_check_tab( $settings ){

  $args = array('post_type' => 'cycle', 'post_status' => 'publish', 'posts_per_page' => -1);
  $allcycles = get_posts( $args );

  $cycles = null;
  if ( count( $allcycles ) > 0 ) {
    foreach ($allcycles as $cycle) {
      $cycles[$cycle->ID] = $cycle->post_title;
    }
  } else {
    $cycles = array( 0 => __( 'No cycle', 'eitagcr' ), );
  }

  $settings['check'] = array(
    'title'       => __( 'Conferência', 'rizoma' ),
    'description' => __( 'Veja as conferências do ciclo', 'rizoma' ),
    'fields'      => array(
      array(
        'id'          => 'cycle_report',
        'label'       => __( 'Cycle', 'eitagcr' ),
        'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
        'type'        => 'select',
        'options'     => $cycles,
      ),
    ),
  );
  return $settings;

}
add_filter( 'eitagcr_settings_fields', 'rizoma_add_check_tab', 10, 1) ;

function load_cycle_checks( $html, $tab ){

  if( $tab == 'check') {
    $cycle_id = get_option( 'eita_gcr_cycle_report' );
    $html .= client_supplier_matrix( $cycle_id );
    $html .= demandante_ofertante_matrix( $cycle_id );
  }

  return $html;
}
add_filter( 'eita_gcr_after_options', 'load_cycle_checks', 10, 2);

function client_supplier_matrix( $cycle_id ){

  # get Orders
  $args = array(
    'post_type'      => 'shop_order',
    'posts_per_page' => -1,
    'post_status'    => ['wc-completed', 'wc-processing', 'wc-on-hold', 'wc-pending'],
    'meta_query'     => array(
      array(
        'key' => '_eita_gcr_cycle_id',
        'value' => $cycle_id,
      ),
      array(
        'key' => 'tipo_de_pedido',
        'value' => 'pedido_demandante',
      ),
    ),
  );
  $orders = get_posts( $args );

  if( ! $orders ) {return "";}

  $customer_supplier = [];
  $suppliers = [];
  $customers = [];

  foreach ($orders as $order) {
    $order = wc_get_order( $order->ID );
    $customer = $order->get_billing_first_name();

    if( ! array_search( $customer, $customers )){
      $customers[] = $customer;
    }

    foreach( $order->get_items() as $item_id => $item ){
      $product_id = $item->get_product_id();
      $supplier = wp_get_post_terms( $product_id, 'supplier' )[0];
      if( ! in_array( $supplier->name, $suppliers)){
        $suppliers[] = $supplier->name;
      }
      $customer_supplier[$customer][$supplier->name] += $item->get_total();
    }
  }

  asort( $suppliers );

  $table = "
    <br / ><br / >
    <h2>Pedidos de Demandantes (sem margem)</h2>
    <table class='customer_supplier'>
    <thead><tr><th></th>
  ";
  foreach ($customers as $customer) {
    $table .= "
      <th>$customer</th>
    ";
  }
  $table .= "
    </tr></thead>
    <tbody>
  ";

  foreach ($suppliers as $supplier) {
    $table .= "
      <tr><th>$supplier</th>
    ";
    foreach ($customers as $customer ) {
      if( $customer_supplier[$customer][$supplier] ){
        $v = wc_price( $customer_supplier[$customer][$supplier] );
      } else {
        $v = "";
      }
      $table .= "
        <td>$v</td>
      ";
    }
    $table .= "
      </tr>
    ";
  }

  $table .= "
    </tbody>
    </table>
  ";

  return $table;
}

function demandante_ofertante_matrix( $cycle_id ){

  # get demandante/ofertante orders
  $args = array(
    'post_type'      => 'shop_order',
    'posts_per_page' => -1,
    'post_status'    => ['wc-completed', 'wc-processing', 'wc-on-hold', 'wc-pending'],
    'meta_query'     => array(
      array(
        'key' => '_eita_gcr_cycle_id',
        'value' => $cycle_id,
      ),
      array(
        'key' => 'tipo_de_pedido',
        'value' => 'pedido_demandante_ofertante',
      ),
    ),
  );
  $orders = get_posts( $args );

  if( ! $orders ) {return "";}

  $suppliers_sum = [];
  $suppliers = [];

  foreach ($orders as $order) {
    $order = wc_get_order( $order->ID );
    // $customer_supplier[$customer] = [];

    foreach( $order->get_items() as $item_id => $item ){
      $product_id = $item->get_product_id();
      $supplier = wp_get_post_terms( $product_id, 'supplier' )[0];
      if( ! in_array( $supplier->name, $suppliers)){
        $suppliers[] = $supplier->name;
      }
      if( ! isset( $suppliers_sum[$supplier->name] )){
        $suppliers_sum[$supplier->name] = 0;
      }
      $suppliers_sum[$supplier->name] += $item->get_total();
    }
  }

  asort( $suppliers );

  # get ofertante orders
  $args = array(
    'post_type'      => 'shop_order',
    'posts_per_page' => -1,
    'post_status'    => ['wc-completed', 'wc-processing', 'wc-on-hold', 'wc-pending'],
    'meta_query'     => array(
      array(
        'key' => '_eita_gcr_cycle_id',
        'value' => $cycle_id,
      ),
      array(
        'key' => 'tipo_de_pedido',
        'value' => 'pedido_ofertante',
      ),
    ),
  );
  $orders = get_posts( $args );

  $suppliers_total = [];

  foreach ($orders as $order) {
    $order = wc_get_order( $order->ID );
    $supplier = wp_get_post_terms( $order->get_id(), 'supplier' )[0]->name;
    $suppliers_total[$supplier] = $order->get_total();
  }

  $table = "
    <br / ><br / >
    <h2>Pedidos de Demandantes/Ofertantes e Pedidos Ofertantes</h2>
    <table class='customer_supplier'>
    <thead>
      <tr>
        <th>Ofertante</th>
        <th>Soma dos Pedidos Demandante/Ofertante</th>
        <th>Valor do Pedido Ofertante</th>
      </tr>
    </thead>
    <tbody>
  ";

  foreach ($suppliers as $supplier) {
    $table .= "
      <tr>
        <th>$supplier</th>
        <td>" . wc_price( $suppliers_sum[ $supplier ] ) . "</td>
        <td>" . wc_price( $suppliers_total[ $supplier ] ) . "</td>
      </tr>
    ";
  }

  $table .= "
    </tbody>
    </table>
  ";

  return $table;
}
