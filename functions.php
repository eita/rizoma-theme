<?php
include( 'functions_checks.php' );
include( 'functions_reports.php' );

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    $parenthandle = 'blocksy';
    $theme = wp_get_theme();
    wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css',
        array(),
        $theme->parent()->get('Version')
    );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array( $parenthandle ),
        $theme->get('Version') // this only works if you have Version in the style header
    );
}

function admin_style() {
  wp_enqueue_style( 'rizoma-admin', get_stylesheet_directory_uri() . '/style_admin.css' );
}
add_action('admin_enqueue_scripts', 'admin_style');

/*
** Registra a taxonomia supplier para representar os ofertantes
*/

function rizoma_register_supplier() {

	$labels = array(
		'name'                       => _x( 'Ofertantes', 'Taxonomy General Name', 'rizoma' ),
		'singular_name'              => _x( 'Ofertante', 'Taxonomy Singular Name', 'rizoma' ),
		'menu_name'                  => __( 'Ofertantes', 'rizoma' ),
		'all_items'                  => __( 'Todos os ofertantes', 'rizoma' ),
		'parent_item'                => __( 'Parent Item', 'rizoma' ),
		'parent_item_colon'          => __( 'Parent Item:', 'rizoma' ),
		'new_item_name'              => __( 'Novo ofertante', 'rizoma' ),
		'add_new_item'               => __( 'Novo ofertante', 'rizoma' ),
		'edit_item'                  => __( 'Editar ofertante', 'rizoma' ),
		'update_item'                => __( 'Atualizar ofertante', 'rizoma' ),
		'view_item'                  => __( 'Ver ofertante', 'rizoma' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'rizoma' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'rizoma' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'rizoma' ),
		'popular_items'              => __( 'Popular Items', 'rizoma' ),
		'search_items'               => __( 'Search Items', 'rizoma' ),
		'not_found'                  => __( 'Not Found', 'rizoma' ),
		'no_terms'                   => __( 'No items', 'rizoma' ),
		'items_list'                 => __( 'Items list', 'rizoma' ),
		'items_list_navigation'      => __( 'Items list navigation', 'rizoma' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'supplier', array( 'product', 'shop_order' ), $args );

}
add_action( 'init', 'rizoma_register_supplier', 0 );

/*
** Insere um título e identificação na tela de pedido
*/

add_action( 'edit_form_top' , 'insert_order_title', 10, 1);
function insert_order_title( $post ){
  $order = wc_get_order( $post->ID );
  if( 'shop_order' === $post->post_type ){
    $tipo_de_pedido = get_post_meta( $post->ID, 'tipo_de_pedido', true );
    $cycle_title = get_the_title( get_post_meta( $post->ID, '_eita_gcr_cycle_id', true ));
    if( 'pedido_demandante_ofertante' === $tipo_de_pedido ){
      $supplier = wp_get_post_terms( $post->ID, 'supplier', [ 'fields' => 'names' ] )[0];
      echo "
        <div class='identificacao_pedido'>
          <p>Pedido Demandante/Ofertante</p>
          <p><b>Demandante:</b> " . $order->get_billing_first_name() . "<br />
          <b>Ofertante:</b> $supplier</p>
          <b>Ciclo:</b> $cycle_title</p>
        </div>
      ";
    } elseif( 'pedido_demandante' === $tipo_de_pedido ){
      echo "
        <div class='identificacao_pedido'>
          <p>Pedido Demandante</p>
          <p><b>Demandante:</b> " . $order->get_billing_first_name() . "</p>
          <b>Ciclo:</b> $cycle_title</p>
        </div>
      ";
    } elseif( 'pedido_ofertante' === $tipo_de_pedido ){
      $supplier = wp_get_post_terms( $post->ID, 'supplier', [ 'fields' => 'names' ] )[0];
      echo "
        <div class='identificacao_pedido'>
          <p>Pedido Ofertante</p>
          <p><b>Ofertante:</b> $supplier</p>
          <p><b>Ciclo:</b> $cycle_title</p>
        </div>
      ";
    }

  }
}

/*
** Remove os metabox desnecessários na tela de pedido
*/

add_filter( 'hidden_meta_boxes', 'hide_supplier_customer_order', 1, 2 );
function hide_supplier_customer_order( $hidden, $screen ){
  global $post;
  if( "shop_order" === $screen->id ){

    if( ! in_array( 'wpo_wcpdf-data-input-box', $hidden )){
      $hidden[] = 'wpo_wcpdf-data-input-box';
    }
    if( ! in_array( 'woocommerce-order-downloads', $hidden )){
      $hidden[] = 'woocommerce-order-downloads';
    }

    // $tipo_de_pedido = get_post_meta( $post->ID, 'tipo_de_pedido', true );
    // if( 'pedido_demandante' === $tipo_de_pedido ){
    //   if( ! in_array( 'supplierdiv', $hidden )){
    //     $hidden[] = 'supplierdiv';
    //   }
    // } else {
    //   unset( $hidden['supplierdiv'] );
    // }
  }
  return $hidden;
}

/*
** Remove o campo de sobrenome e empresa na página minha conta > endereços
*/

add_filter( 'woocommerce_default_address_fields', 'custom_default_address_fields' );
function custom_default_address_fields( $fields ) {
    unset( $fields[ 'company' ]);
    unset( $fields[ 'last_name' ]);
    unset( $fields[ 'country' ]);
    return $fields;
}

/*
** Remove a obrigatoriedade do campo de sobrenome e login na página detalhes da conta (edit-account)
*/

add_filter('woocommerce_save_account_details_required_fields', 'ts_hide_last_name');
function ts_hide_last_name($required_fields)
{
  unset($required_fields["account_last_name"]);
  unset($required_fields["account_display_name"]);
  return $required_fields;
}

/*
** Seleciona o tipo de margem de acordo com atributo do usuário
*/

add_action( 'woocommerce_cart_calculate_fees', 'rizoma_adiciona_margem' );
function rizoma_adiciona_margem( $cart ) {
    if ( is_admin() && !defined('DOING_AJAX') )
        return;

    $data = WC()->session->get('fee_data');
    $fee  = $total = 0;

    $current_user = wp_get_current_user();

    [$rate, $name] = rizoma_get_fee( $current_user->ID );

    $total = 0;
    foreach ( $cart->get_cart() as $cart_item ) {
      $total += $cart_item['line_total'];
    }

    $fee = $total * $rate / 100;

    // Add the fee
    if ( $fee > 0 ) {
        $cart->add_fee( $name, $fee );
    }
}

/*
** Seta tipo de pedido ao criar um novo pedido
*/

function set_tipo_de_pedido( $order_id ){
    ///TODO: Verificar qual condição se aplicar para que pedido seja ao ofertante
    update_post_meta( $order_id, 'tipo_de_pedido', 'pedido_demandante' );
}
add_action( 'woocommerce_new_order', 'set_tipo_de_pedido' );

/*
** Adiciona filtro no admin para Pedidos de Demandantes e Ofertantes
*/

function add_filter_to_order_admin(){

    global $post_type;
    if($post_type == 'shop_order'){

      // Filtra por tipo de pedido
      $selecteddem = "";
      $selecteddemofer = "";
      $selectedofer = "";
      if(isset($_GET['tipo_de_pedido'])){
          $selected = sanitize_text_field($_GET['tipo_de_pedido']);
          if ( $selected == 'pedido_demandante' ) { $selecteddem = 'selected=selected'; }
          if ( $selected == 'pedido_demandante_ofertante' ) { $selecteddemofer = 'selected=selected'; }
          if ( $selected == 'pedido_ofertante' ) { $selectedofer = 'selected=selected'; }
      }

      $html = "
      <select name='tipo_de_pedido' id='tipo_de_pedido' class='postform'>
        <option class='level-0' value=''>" . __('Todos os tipos de pedidos', 'eitagcr') . "</option>
	      <option class='level-0' $selecteddem value='pedido_demandante'>Pedido do Demandante</option>
        <option class='level-0' $selecteddemofer value='pedido_demandante_ofertante'>Pedido Demandante/Ofertante</option>
        <option class='level-0' $selectedofer value='pedido_ofertante'>Pedido ao Ofertante</option>
      </select>
      ";
    }

    // Filtra por fornecedor
    if(isset($_GET['supplier'])){
      $selected = sanitize_text_field($_GET['supplier']);
    } else {
      $selected = NULL;
    }

    $options = get_terms( array( 'taxonomy' => 'supplier', 'hide_empty' => true) );

    $html .= "
    <select name='supplier' id='supplier' class='postform'>
      <option class='level-0' value=''>" . __('Todos os ofertantes', 'eitagcr') . "</option>
    ";

    foreach ($options as $option) {
      if ( $option->slug === $selected ) {
        $isselected = "selected=selected";
      } else {
        $isselected = "";
      }

      $html .= "
      <option class='level-0' value=$option->slug $isselected>$option->name</option>
      ";
    }

    $html .= "</select>";

    // Filtra por ciclo
    $selectedcycle = "";
    if(isset($_GET['cycle_id'])){
        $selected = sanitize_text_field($_GET['cycle_id']);
    }

    $html .= "
    <select name='cycle_id' id='cycle' class='postform'>
    ";
    $args = array(
      'post_type'      => 'cycle',
      'posts_per_page' => -1,
    );
    $cycles = get_posts( $args );

    $html .= "<option class='level-0' value=''>" . __('Todos os ciclos', 'eitagcr') . "</option>";
    foreach ($cycles as $cycle) {
      if( $selected == $cycle->ID ){
        $sel = "selected=selected";
      } else {
        $sel = "";
      }
      $html .= "<option $sel class='level-0' value='".$cycle->ID."'>" . $cycle->post_title . "</option>";
    }
    $html .= "
    </select>
    ";

    echo $html;
}
add_action('restrict_manage_posts','add_filter_to_order_admin');

/*
** Filtra os posts no admin pelo filtro escolhido
*/

function filter_orders_by_meta( $query ){
    global $pagenow;
    $type = 'shop_order';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }
    if ( 'shop_order' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['tipo_de_pedido']) && $_GET['tipo_de_pedido'] != '' && $query->is_main_query()) {
      $meta_query = (array)$query->get('meta_query');
      $meta_query[] = array(
        'key'     => 'tipo_de_pedido',
        'value'   => $_GET['tipo_de_pedido'],
      );
      $query->set('meta_query',$meta_query);
    }

    if ( 'shop_order' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['cycle_id']) && $_GET['cycle_id'] != '' && $query->is_main_query()) {
      $meta_query = (array)$query->get('meta_query');
      $meta_query[] = array(
        'key'     => '_eita_gcr_cycle_id',
        'value'   => $_GET['cycle_id'],
      );
      $query->set('meta_query',$meta_query);
    }
}
add_action( 'pre_get_posts', 'filter_orders_by_meta' );

// Inserindo após a coluna do ciclo. O ID se refere ao plugin Admin Columns.
add_filter( 'manage_edit-shop_order_columns', 'rizoma_tipo_de_pedido_add_column', 99999 );
function rizoma_tipo_de_pedido_add_column( $columns ) {
  $columns = array_insert_after( 'order_number', $columns, 'ciclo', 'Ciclo');
  //$columns = array_insert_after( '605d43a467e6e', $columns, 'ciclo', 'Ciclo');
  $columns = array_insert_after( 'ciclo', $columns, 'tipo_de_pedido', 'Tipo de Pedido');
	return $columns;
}



// Para pegar o label diretamente do ACF, é preciso descobrir como gravar os campos ao fazer o comando create_order. Os campos do ACF só gravam pelo ACF ao editar o order e clicar em atualzar.
add_action('manage_shop_order_posts_custom_column', 'rizoma_tipo_de_pedido_column_content', 10, 2);
function rizoma_tipo_de_pedido_column_content( $column_key, $post_id ){
  if( 'tipo_de_pedido' === $column_key ){
    $tipo_de_pedido = get_post_meta( $post_id, 'tipo_de_pedido', true );
    switch( $tipo_de_pedido ){
      case 'pedido_demandante':
        echo 'Demandante'; break;
      case 'pedido_demandante_ofertante':
        echo 'Demandante/Ofertante'; break;
      case 'pedido_ofertante':
        echo 'Ofertante'; break;
    }
  }

  if( 'ciclo' === $column_key ){
    $ciclo = get_post_meta( $post_id, '_eita_gcr_cycle_id', true );
    $ciclo = get_post( $ciclo );
    echo $ciclo->post_title;
  }

}




/*
** Adiciona taxonomia customizada ao importador
*/
function add_column_to_importer( $options ) {

        $options['supplier'] = __( 'Supplier', 'eitagcr' );
        return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_column_to_importer' );

/**
 * Add automatic mapping support for 'Custom Column'.
 * This will automatically select the correct mapping for columns named 'Custom Column' or 'custom column'.
 *
 * @param array $columns
 * @return array $columns
 */
function add_column_to_mapping_screen( $columns ) {

        $columns['Supplier'] = 'supplier';
        $columns['Ofertante'] = 'supplier';

        return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_column_to_mapping_screen' );

/**
 * Process the data read from the CSV file.
 * This just saves the value in meta data, but you can do anything you want here with the data.
 *
 * @param WC_Product $object - Product being imported or updated.
 * @param array $data - CSV data read for the product.
 * @return WC_Product $object
 */
function process_import( $object, $data ) {

        if ( ! empty( $data['supplier'] ) ) {
                $terms = explode(", ", $data['supplier']);
                $term_slugs = [];
                foreach( $terms as $term_name){
                        $term_slugs[] = sanitize_title( $term_name );
                }
                $r = wp_set_object_terms( $object->get_id(), $term_slugs, 'supplier' );
        }

        return $object;
}
add_action( 'woocommerce_product_import_inserted_product_object', 'process_import', 10, 2 );

function add_cycle_buttons( $html, $tab ){
  if( $tab == 'cycles') {

    $cycle_id = get_option( 'eita_gcr_cycle_to_configure' );
    $orders_split = get_option( 'rizoma_orders_split_' . $cycle_id );
    if( $orders_split ){
      $disabled = 'disabled';
      $icon = "<span class='dashicons dashicons-yes-alt'></span>";
    } else {
      $disabled = '';
      $icon = "";
    }

    $html .= '<form id="split_client_orders" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles">
      <input form="split_client_orders" type="hidden" name="split_client_orders" value="yes">
      <input class="button-secondary" form="split_client_orders" type="submit" value="' . __('Separar pedidos dos demandantes por ofertantes (pedidos cruzados)', 'eitagcr') . '" ' . $disabled . ' >
      <label for="split_client_orders"><span class="description">' . $icon . ' '. __("Após o termino do ciclo, clique aqui para gerar os pedidos separados de cada demandante para cada ofertante.", "eitagcr").'</span></label>
    </form><br />';

    $orders_split = get_option( 'rizoma_supplier_orders_create_' . $cycle_id );
    if( $orders_split ){
      $icon = "<span class='dashicons dashicons-yes-alt'></span>";
    } else {
      $icon = "";
    }

    $html .= '<form id="create_supplier_orders" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles">
      <input form="create_supplier_orders" type="hidden" name="create_supplier_orders" value="yes">
      <input class="button-secondary" form="create_supplier_orders" type="submit" value="' . __('Criar/Atualizar pedidos aos ofertantes', 'eitagcr') . '">
      <label for="create_supplier_orders"><span class="description">' . $icon . ' '. __("Após o termino do ciclo, clique aqui para gerar os pedidos totais aos ofertantes.", "eitagcr").'</span></label>
    </form><br />';

    $orders_split = get_option( 'rizoma_clients_orders_update_' . $cycle_id );
    if( $orders_split ){
      $icon = "<span class='dashicons dashicons-yes-alt'></span>";
    } else {
      $icon = "";
    }

    $html .= '<form id="update_clients_orders" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles">
      <input form="update_clients_orders" type="hidden" name="update_clients_orders" value="yes">
      <input class="button-secondary" form="update_clients_orders" type="submit" value="' . __('Atualizar pedidos demandante/ofertante', 'eitagcr') . '">
      <label for="update_clients_orders"><span class="description">' . $icon . ' '. __("Após confirmação dos tributos e frete nos pedidos aos ofertantes, clique aqui para atualizar os pedidos demandate/ofertante com o valor adicional.", "eitagcr").'</span></label>
    </form><br />';

    $html .= '<form id="reset_orders" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles">
      <input form="reset_orders" type="hidden" name="reset_orders" value="yes">
      <input class="button-secondary" form="reset_orders" type="submit" value="' . __('Reiniciar processo do ciclo', 'eitagcr') . '">
      <label for="reset_orders"><span class="description">'. __("ATENÇÃO: apaga todos os pedidos criados pelo sistema, deixando apenas os pedidos originais dos demandantes.", "eitagcr").'</span></label>
    </form><br />';

    $html .= '<form id="update_prices" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles">
      <input form="update_prices" type="hidden" name="update_prices" value="yes">
      <input class="button-secondary" form="update_prices" type="submit" value="' . __('Atualizar preços', 'eitagcr') . '">
      <label for="update_prices"><span class="description">'. __("Atualiza nos pedidos deste ciclo os preços de acordo com o preço atual do produto no sistema.", "eitagcr").'</span></label>
    </form>';

  }
  return $html;
}
add_filter( 'eita_gcr_after_options', 'add_cycle_buttons', 10, 2);

/*
** Esta funcionalidade reparte o pedido do demandante em vários pedidos de demandante para cada ofertante. Ver #13.
*/
function rizoma_split_orders(){
  if ( isset( $_POST[ 'split_client_orders' ] ) && $_POST[ 'split_client_orders' ] ) {

    unhook_emails();

    $cycle_id = get_option( 'eita_gcr_cycle_to_configure' );

    # get Orders
    $args = array(
      'post_type'      => 'shop_order',
      'posts_per_page' => -1,
      'post_status'    => 'wc-processing',
      'meta_query'     => array(
        array(
          'key' => '_eita_gcr_cycle_id',
          'value' => $cycle_id,
        ),
        array(
          'key' => 'tipo_de_pedido',
          'value' => 'pedido_demandante',
        ),
      ),
    );
    $orders = get_posts( $args );

    foreach ($orders as $original_order) {
      # get order data
      $original_order = wc_get_order( $original_order->ID );
      $billing_address = array(
          'first_name' => $original_order->get_billing_first_name(),
          'last_name'  => $original_order->get_billing_last_name(),
          'company'    => $original_order->get_billing_company(),
          'email'      => $original_order->get_billing_email(),
          'phone'      => $original_order->get_billing_phone(),
          'address_1'  => $original_order->get_billing_address_1(),
          'address_2'  => $original_order->get_billing_address_2(),
          'city'       => $original_order->get_billing_city(),
          'state'      => $original_order->get_billing_state(),
          'postcode'   => $original_order->get_billing_postcode(),
          'country'    => $original_order->get_billing_country(),
      );

      # get all items in order per supplier;
      $user_id = $original_order->get_user()->ID;
      $suppliers_items = [];
      foreach( $original_order->get_items() as $item_id => $item ){
        $product_id = $item->get_product_id();
        $supplier = wp_get_post_terms( $product_id, 'supplier' )[0];
        $suppliers_items[$supplier->term_id][] = $item;
      }

      # foreach supplier, create an order;
      foreach ( $suppliers_items as $supplier => $items ) {
        $order = new WC_Order();
        $order->set_status( 'on-hold' );
        $order->user_id = $user_id;
        $order->set_address( $billing_address, 'billing' );
        $order->set_payment_method( $original_order->get_payment_method() );
        $total = 0;
        foreach ($items as $item) {
          $order->add_product( $item->get_product(), $item->get_quantity());
          $total += $item->get_total();
        }
        $order->calculate_totals();

        $fee_item = new WC_Order_Item_Fee();
        [$rate, $name] = rizoma_get_fee( $user_id );
        $fee_item->set_name( $name );
		    $fee_item->set_total( $order->get_total() * $rate / 100 );
        $order->add_item( $fee_item );

        $order->calculate_totals();
        $order->save();
        update_post_meta( $order->get_id(), '_customer_user', $user_id );
        update_post_meta( $order->get_id(), 'tipo_de_pedido', 'pedido_demandante_ofertante' );
        update_post_meta( $order->get_id(), '_eita_gcr_cycle_id', $cycle_id );
        wp_set_post_terms($order->get_id(), $supplier, 'supplier', false);
      }

    }

    update_option( 'rizoma_orders_split_' . $cycle_id, 1 );

    add_action( 'admin_notices', 'rizoma_split_orders_notice' );
  }
}
add_action( 'eita_gcr_process_request', 'rizoma_split_orders' );

function rizoma_split_orders_notice() {
  ?>
    <div class="notice notice-success is-dismissible">
      <p><?php _e( 'Pedidos dos demandantes separados com sucesso.', 'rizoma' ); ?></p>
    </div>
  <?php
}
/*
** Esta funcionalidade cria os pedidos aos ofertantes. Ver #16.
*/
function rizoma_create_supplier_orders(){
  if ( isset( $_POST[ 'create_supplier_orders' ] ) && $_POST[ 'create_supplier_orders' ] ) {

    unhook_emails();

    $cycle_id = get_option( 'eita_gcr_cycle_to_configure' );

    # get suppliers
    $suppliers = get_terms( array( 'taxonomy' => 'supplier', 'hide_empty' => True ));

    foreach ($suppliers as $supplier) {

      # get Orders
      $args = array(
        'post_type'      => 'shop_order',
        'posts_per_page' => -1,
        'post_status'    => ['wc-on-hold'],
        'meta_query'     => array(
          'relation' => 'AND',
          array(
            'key' => '_eita_gcr_cycle_id',
            'value' => $cycle_id,
          ),
          array(
            'key' => 'tipo_de_pedido',
            'value' => 'pedido_demandante_ofertante',
          ),
        ),
        'tax_query' => array(
          array(
            'field' => 'slug',
            'taxonomy' => 'supplier',
            'terms' => $supplier->slug
          )
        ),
      );
      $orders = get_posts( $args );

      if( ! $orders ) {continue;}

      $items = [];
      foreach ($orders as $order) {
        $order = wc_get_order( $order->ID );
        foreach ($order->get_items() as $item) {
          $id = $item->get_product_id();
          # verifica se o produto já está no array
          if( array_key_exists( $id, $items )){
            $items[$id]->set_quantity( $items[$id]->get_quantity() + $item->get_quantity() );
          } else {
            $items[$id] = $item;
          }
        }
      }

      // verfica se pedido já existe; senão cria
      $args = array(
        'post_type'      => 'shop_order',
        'posts_per_page' => -1,
        'post_status'    => ['wc-on-hold', 'wc-processing', 'wc-pending'],
        'meta_query'     => array(
          'relation' => 'AND',
          array(
            'key' => '_eita_gcr_cycle_id',
            'value' => $cycle_id,
          ),
          array(
            'key' => 'tipo_de_pedido',
            'value' => 'pedido_ofertante',
          ),
        ),
        'tax_query' => array(
          array(
            'field' => 'slug',
            'taxonomy' => 'supplier',
            'terms' => $supplier->slug
          )
        ),
      );
      $supplier_order = get_posts( $args );
      if( $supplier_order ){
        $supplier_order = wc_get_order( $supplier_order[0]->ID );
        foreach ($supplier_order->get_items() as $item) {
          $supplier_order->remove_item( $item->get_id() );
        }
      } else {
        $supplier_order = new WC_Order();
      }

      $supplier_order->set_status( 'on-hold' );

      $user = get_user_by( 'login', 'rizoma' );
      $supplier_order->user_id = $user->ID;
      $rizoma_customer = new WC_Customer( $user->ID );
      $data = $rizoma_customer->get_data();

      $billing_address = array(
          'first_name' => $data['billing']['first_name'],
          'last_name'  => $data['billing']['last_name'],
          'email'      => $data['billing']['email'],
          'phone'      => $data['billing']['phone'],
          'address_1'  => $data['billing']['address_1'],
          'address_2'  => $data['billing']['address_2'],
          'city'       => $data['billing']['city'],
          'state'      => $data['billing']['state'],
          'postcode'   => $data['billing']['postcode'],
          'country'    => $data['billing']['country'],
      );

      $supplier_order->set_address( $billing_address, 'billing' );

      foreach ($items as $item) {
        $supplier_order->add_product( $item->get_product(), $item->get_quantity());
      }
      $supplier_order->calculate_totals();
      $supplier_order->save();

      update_post_meta( $supplier_order->get_id(), '_customer_user', $user->ID );
      update_post_meta( $supplier_order->get_id(), 'tipo_de_pedido', 'pedido_ofertante' );
      update_post_meta( $supplier_order->get_id(), '_eita_gcr_cycle_id', $cycle_id );

      wp_set_post_terms($supplier_order->get_id(), $supplier->term_id, 'supplier', false);
    }

    update_option( 'rizoma_supplier_orders_create_' . $cycle_id, 1 );

    add_action( 'admin_notices', 'rizoma_create_supplier_orders_notice' );
  }
}
add_action( 'eita_gcr_process_request', 'rizoma_create_supplier_orders' );

function rizoma_create_supplier_orders_notice() {
  ?>
    <div class="notice notice-success is-dismissible">
      <p><?php _e( 'Pedidos dos ofertantes criados com sucesso.', 'rizoma' ); ?></p>
    </div>
  <?php
}

/*
** Esta funcionalidade atualiza os pedidos dos demandantes após a atualização dos pedidos aos ofertantes. Ver #14.
*/
function rizoma_update_clients_orders(){
  if ( isset( $_POST[ 'update_clients_orders' ] ) && $_POST[ 'update_clients_orders' ] ) {
    $cycle_id = get_option( 'eita_gcr_cycle_to_configure' );
    [$items_extra_cost, $items_cost, $payment_dates] = rizoma_update_clients_orders_get_suppliers_orders( $cycle_id );
    rizoma_update_clients_orders_get_clients_orders( $cycle_id, $items_extra_cost, $items_cost, $payment_dates );
    update_option( 'rizoma_clients_orders_update_' . $cycle_id, 1 );
    add_action( 'admin_notices', 'rizoma_update_clients_orders_notice' );
  }
}
add_action( 'eita_gcr_process_request', 'rizoma_update_clients_orders' );

function rizoma_update_clients_orders_get_suppliers_orders( $cycle_id ){

  # get suppliers orders
  $args = array(
    'post_type'      => 'shop_order',
    'posts_per_page' => -1,
    'post_status'    => ['wc-on-hold', 'wc-pending'],
    'meta_query'     => array(
      'relation' => 'AND',
      array(
        'key' => '_eita_gcr_cycle_id',
        'value' => $cycle_id,
      ),
      array(
        'key' => 'tipo_de_pedido',
        'value' => 'pedido_ofertante',
      ),
    ),
  );
  $suppliers_orders = get_posts( $args );

  # calculate extra cost for each item
  $items_extra_cost = [];
  $items_cost = [];
  $payment_dates = [];

  foreach ( $suppliers_orders as $supplier_order_post ) {

    $supplier_order = wc_get_order( $supplier_order_post->ID );

    # get fee and shipping
    $fee = $supplier_order->get_total_fees(); // taxas
    $shipping = $supplier_order->get_shipping_total(); // entrega

    # get items
    foreach ($supplier_order->get_items( 'line_item' ) as $item) {
      $id = $item->get_product_id();
      $total = $item->get_total();
      $qty = $item->get_quantity();
      $extra_cost = $fee + $shipping;
      $items_extra_cost[$id] = $extra_cost * $total / $qty / ( $supplier_order->get_total() - $extra_cost );
      $items_cost[$id] = $total / $qty;
    }

    // $supplier_order->set_status( 'pending' );
    $supplier_order->save();

    # salvando a data do vencimento do pagamento
    $supplier = wp_get_post_terms( $supplier_order_post->ID, 'supplier' );
    $payment_dates[ $supplier[0]->slug ] = get_field('vencimento_do_pagamento', $supplier_order_post->ID);

  }

  return [$items_extra_cost, $items_cost, $payment_dates];
}

function rizoma_update_clients_orders_get_clients_orders( $cycle_id, $items_extra_cost, $items_cost, $payment_dates ){
  # get clients orders
  $args = array(
    'post_type'      => 'shop_order',
    'posts_per_page' => -1,
    'post_status'    => [ 'wc-on-hold', 'wc-pending' ],
    'meta_query'     => array(
      'relation' => 'AND',
      array(
        'key' => '_eita_gcr_cycle_id',
        'value' => $cycle_id,
      ),
      array(
        'key' => 'tipo_de_pedido',
        'value' => 'pedido_demandante_ofertante',
      ),
    ),
  );
  $client_orders = get_posts( $args );

  foreach ( $client_orders as $client_order_post ) {
    $client_order_id = $client_order_post->ID;
    $client_order = wc_get_order( $client_order_id );
    $user_id = $client_order->get_user()->ID;

    # get and update items
    foreach ($client_order->get_items( 'line_item' ) as $item) {
      $id = $item->get_product_id();
      if( array_key_exists( $id, $items_extra_cost) ){
        $item_quantity = $item->get_quantity();
        $item_cost = $items_cost[$id] * $item_quantity;
        $extra_cost = $items_extra_cost[$id] * $item_quantity;
        $item->set_subtotal( $item_cost + $extra_cost );
        $item->set_total( $item_cost + $extra_cost );
        $item->update_meta_data( 'Adicional', wc_price( $extra_cost ));
      }
    }
    // É necessário remover a margem e inserir de novo (Ver issue #36)
    foreach ($client_order->get_items( 'fee' ) as $item) {
      $client_order->remove_item( $item->get_id() );
    }
    $client_order->calculate_totals( false );
    $fee_item = new WC_Order_Item_Fee();
    [$rate, $name] = rizoma_get_fee( $user_id );
    $fee_item->set_name( $name );
    $fee_item->set_total( $client_order->get_total() * $rate / 100 );
    $client_order->add_item( $fee_item );
    // $client_order->set_status( 'pending' );
    $client_order->calculate_totals( false );
    $client_order->save();
    # atualizando a data do vencimento do pagamento, com 3 dias antes #25
    $supplier = wp_get_post_terms( $client_order_id, 'supplier' );
    if( $payment_dates[ $supplier[0]->slug ] ){
      update_post_meta( $client_order_id, 'vencimento_do_pagamento', date_i18n('Ymd', strtotime( '-3 days', strtotime( $payment_dates[ $supplier[0]->slug ] ))));
    }
  }
}


function rizoma_update_clients_orders_notice() {
  ?>
    <div class="notice notice-success is-dismissible">
      <p><?php _e( 'Pedidos demandante/ofertante atualizados com sucesso.', 'rizoma' ); ?></p>
    </div>
  <?php
}

/*
** Esta funcionalidade atualiza os pedidos dos demandantes após a atualização dos pedidos aos ofertantes. Ver #14.
*/
function rizoma_reset_orders(){
  if ( isset( $_POST[ 'reset_orders' ] ) && $_POST[ 'reset_orders' ] ) {

    $cycle_id = get_option( 'eita_gcr_cycle_to_configure' );

    # get all orders but original ones
    $args = array(
      'post_type'      => 'shop_order',
      'posts_per_page' => -1,
      'post_status'    => ['wc-on-hold', 'wc-processing', 'wc-pending', 'wc-cancelled', 'wc-completed'],
      'meta_query'     => array(
        'relation' => 'AND',
        array(
          'key' => '_eita_gcr_cycle_id',
          'value' => $cycle_id,
        ),
        array(
          'key' => 'tipo_de_pedido',
          'value' => 'pedido_demandante',
          'compare' => 'NOT IN'
        ),
      ),
    );
    $orders_to_delete = get_posts( $args );


    foreach ( $orders_to_delete as $order ) {
      wp_delete_post( $order->ID, true );
    }

    update_option( 'rizoma_orders_split_' . $cycle_id, 0 );
    update_option( 'rizoma_supplier_orders_create_' . $cycle_id, 0 );
    update_option( 'rizoma_clients_orders_update_' . $cycle_id, 0 );

    add_action( 'admin_notices', 'rizoma_reset_orders_notice' );
  }
}
add_action( 'eita_gcr_process_request', 'rizoma_reset_orders' );

function rizoma_reset_orders_notice() {
  ?>
    <div class="notice notice-success is-dismissible">
      <p><?php _e( 'Pedidos criados pelo sistema apagados com sucesso. Agora você pode reiniciar o processo do ciclo.', 'rizoma' ); ?></p>
    </div>
  <?php
}


function array_insert_after($key, array &$array, $new_key, $new_value) {
  if (array_key_exists($key, $array)) {
    $new = array();
    foreach ($array as $k => $value) {
      $new[$k] = $value;
      if ($k === $key) {
        $new[$new_key] = $new_value;
      }
    }
    return $new;
  }
  return FALSE;
}

function rizoma_get_fee( $user_id ){

  $tipo_de_demandante = get_user_meta( $user_id, 'tipo_de_demandante', true );

  switch( $tipo_de_demandante ){
    case 'Empreendimento de Economia Solidária':
      $rate = 3;
      $name = "Margem Rizoma EES (3%)";
      break;
    case 'Empreendimento Solidário Familiar':
      $rate = 12;
      $name = "Margem Rizoma ESF (12%)";
      break;
    case 'Grupo de Consumo Responsável':
      $rate = 6;
      $name = "Margem Rizoma GCR (6%)";
      break;
    case 'Extravio':
      $rate = 0;
      $name = "Margem Rizoma Extravio (0%)";
      break;
    default:
      $rate = 0;
      $name = "";
  }

  return [$rate, $name];

}

function change_translation( $translated, $original, $domain ){
  if( strpos( $translated, 'taxa' )){
    $translated = str_replace( 'taxa', 'tributo', $translated );
  }
  if( $translated === 'Taxas:' ){
    global $post;
    $tipo_de_pedido = get_post_meta( $post->ID, 'tipo_de_pedido', true );
    if( 'pedido_ofertante' == $tipo_de_pedido ){
      $translated = "Tributos:";
    } else {
      $translated = "Margem:";
    }
  }
  return $translated;
}
add_filter( 'gettext', 'change_translation', 10, 3 );

function unhook_emails(){

  /**
   * Unhook and remove WooCommerce default emails.
   */
  add_action( 'woocommerce_email', 'unhook_those_pesky_emails' );

  function unhook_those_pesky_emails( $email_class ) {

  		/**
  		 * Hooks for sending emails during store events
  		 **/
  		remove_action( 'woocommerce_low_stock_notification', array( $email_class, 'low_stock' ) );
  		remove_action( 'woocommerce_no_stock_notification', array( $email_class, 'no_stock' ) );
  		remove_action( 'woocommerce_product_on_backorder_notification', array( $email_class, 'backorder' ) );

  		// New order emails
  		remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
  		remove_action( 'woocommerce_order_status_pending_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
  		remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
  		remove_action( 'woocommerce_order_status_failed_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
  		remove_action( 'woocommerce_order_status_failed_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
  		remove_action( 'woocommerce_order_status_failed_to_on-hold_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );

  		// Processing order emails
  		remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );
  		remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_Customer_On_Hold_Order'], 'trigger' ) );

  		// Completed order emails
  		remove_action( 'woocommerce_order_status_completed_notification', array( $email_class->emails['WC_Email_Customer_Completed_Order'], 'trigger' ) );

  		// Note emails
  		remove_action( 'woocommerce_new_customer_note_notification', array( $email_class->emails['WC_Email_Customer_Note'], 'trigger' ) );
  }

}


add_action( 'woocommerce_after_order_object_save', 'rizoma_sort_order_items', 10, 2 );
function rizoma_sort_order_items( $order, $data_store ){

  remove_action( 'woocommerce_after_order_object_save', 'rizoma_sort_order_items', 10 );

  $order_items = $order->get_items();
  $new_order_items = [];
  foreach ( $order_items as $order_item ) {
    $product_id = $order_item->get_product_id();
    $supplier = wp_get_post_terms( $product_id, 'supplier' );
    if( $supplier ){
      $supplier = $supplier[0]->name;
    } else {
      $supplier = "Sem fornecedor";
    }
    $new_order_items[ $supplier ][] = $order_item;
    $order->remove_item( $order_item->get_ID() );
  }

  $suppliers = array_unique( array_keys( $new_order_items ));
  asort( $suppliers );
  foreach ($suppliers as $supplier) {
    usort( $new_order_items[ $supplier ], 'rizoma_order_by_name' );
    foreach ($new_order_items[ $supplier ] as $order_item) {
      $order->add_item( $order_item );
    }
  }
  $order->save();

}

function rizoma_order_by_name( $item_a, $item_b )
{
    return strcmp(strtolower($item_a->get_name()),strtolower($item_b->get_name()));
}


// Recalcula a margem rizoma para pedidos demandante ou pedido_demandante_ofertante após possíveis alterações no pedido. Ver #36
add_action( 'woocommerce_after_order_object_save', 'rizoma_recalculate_fee', 10, 2 );
function rizoma_recalculate_fee( $order, $data_store ){

  remove_action( 'woocommerce_after_order_object_save', 'rizoma_recalculate_fee', 10 );

  $tipo_de_pedido = get_post_meta( $order->get_ID(), 'tipo_de_pedido', true );
  if( 'pedido_demandante' === $tipo_de_pedido || 'pedido_demandante_ofertante' === $tipo_de_pedido){
    // É necessário remover a margem e inserir de novo (Ver issue #36)
    $user = $order->get_user();
    if( $user ){
      $user_id = $user->ID;
    } else {
      return;
    }

    foreach ($order->get_items( 'fee' ) as $item) {
      $order->remove_item( $item->get_id() );
    }

    $order->calculate_totals();
    $fee_item = new WC_Order_Item_Fee();
    [$rate, $name] = rizoma_get_fee( $user_id );
    $fee_item->set_name( $name );
    $fee_item->set_total( $order->get_total() * $rate / 100 );
    $order->add_item( $fee_item );
    $order->calculate_totals();
    $order->save();
  }
  add_action( 'woocommerce_after_order_object_save', 'rizoma_recalculate_fee', 10, 2 );
}

add_action( 'eita_gcr_process_request', 'rizoma_update_prices' );
function rizoma_update_prices(){

  if ( isset( $_POST[ 'update_prices' ] ) && $_POST[ 'update_prices' ] ) {

    $cycle_id = get_option( 'eita_gcr_cycle_to_configure' );
    $args = array(
      'post_type'      => 'shop_order',
      'posts_per_page' => -1,
      'post_status'    => ['wc-processing', 'wc-on-hold', 'wc-pending'],
      'meta_query'     => array(
        array(
          'key' => '_eita_gcr_cycle_id',
          'value' => $cycle_id,
        ),
      ),
    );
    $orders = get_posts( $args );

    foreach ($orders as $order) {
      do_update_price( $order );
    }
    add_action( 'admin_notices', 'rizoma_update_prices_notice' );
  }
}

function do_update_price( $order ){
  $order = wc_get_order( $order->ID );
  $items = $order->get_items();
  foreach ( $items as $item ) {
    $product = $item->get_product();
    $unit_price = $item->get_total() / $item->get_quantity();
    if( (float)$product->get_price() != $unit_price ){
      $item->set_subtotal( $item->get_quantity() * $product->get_price() );
      $item->set_total( $item->get_quantity() * $product->get_price() );
      $item->save();
    }
  }
  $order->calculate_totals();
  $order->save();
}

function rizoma_update_prices_notice() {
  ?>
    <div class="notice notice-success is-dismissible">
      <p><?php _e( 'Preços atualizados com sucesso.', 'rizoma' ); ?></p>
    </div>
  <?php
}

function print_mem()
{
   /* Currently used memory */
   $mem_usage = memory_get_usage();

   /* Peak memory usage */
   $mem_peak = memory_get_peak_usage();

   error_log('The script is now using: ' . round($mem_usage / 1024) . 'KB of memory.');
   // error_log('Peak usage: ' . round($mem_peak / 1024) . 'KB of memory.');
}
