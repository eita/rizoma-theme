# Rizoma Bem da Terra

Este repositório contém o tema-filho para o sistema Rizoma Bem da Terra de consumo responsável. 

Este sistema tem como objetivo ser uma ferramenta de operação das compras da Rede Rizoma.

## Requistos

Além deste tema filho, o sistema tem como requistos:

* [Tema Blocksy](https://wordpress.org/themes/blocksy/)
* [Plugin Woocommerce](https://wordpress.org/plugins/woocommerce/)
* [Plugin EITA Commerce](https://gitlab.com/eita/eitacommerce/)
* [Plugin EITA GCR](https://gitlab.com/eita/eitagcr/)
