<?php

add_filter( 'eitagcr_settings_fields', 'rizoma_add_report_tabs', 10, 1) ;
function rizoma_add_report_tabs( $settings ){

  // remove standard reports
  unset( $settings['report_1'] );
  unset( $settings['report_2'] );
  unset( $settings['report_3'] );
  unset( $settings['report_4'] );
  unset( $settings['report_5'] );
  unset( $settings['report_6'] );


  $args = array('post_type' => 'cycle', 'post_status' => 'publish', 'posts_per_page' => -1);
  $allcycles = get_posts( $args );

  $cycles = null;
  if ( count( $allcycles ) > 0 ) {
    foreach ($allcycles as $cycle) {
      $cycles[$cycle->ID] = $cycle->post_title;
    }
  } else {
    $cycles = array( 0 => __( 'No cycle', 'eitagcr' ), );
  }

  $settings['rizoma_report_1'] = array(
    'title'       => __( 'Relatório contábil - ciclo', 'rizoma' ),
    'description' => __( '', 'rizoma' ),
    'fields'      => array(
      array(
        'id'          => 'cycle_report',
        'label'       => __( 'Cycle', 'eitagcr' ),
        'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
        'type'        => 'select',
        'options'     => $cycles,
      ),
    ),
  );

  $settings['rizoma_report_2'] = array(
    'title'       => __( 'Relatório contábil - mais de um ciclo', 'rizoma' ),
    'description' => __( '', 'rizoma' ),
    'fields'      => array(
      array(
        'id'          => 'cycle_accounting_report',
        'label'       => __( 'Cycle', 'eitagcr' ),
        'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
        'type'        => 'checkbox_multi',
        'options'     => $cycles,
      ),
    ),
  );

  $settings['rizoma_report_3'] = array(
    'title'       => __( 'Livro Caixa', 'rizoma' ),
    'description' => __( 'Livro Caixa', 'rizoma' ),
  );

  $settings['rizoma_report_4'] = array(
    'title'       => __( 'Controle de pagamentos - Receitas', 'rizoma' ),
    'description' => __( '', 'rizoma' ),
    'fields'      => array(
      array(
        'id'          => 'cycle_report',
        'label'       => __( 'Cycle', 'eitagcr' ),
        'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
        'type'        => 'select',
        'options'     => $cycles,
      ),
    ),
  );

  $settings['rizoma_report_5'] = array(
    'title'       => __( 'Controle de pagamentos - Despesas', 'rizoma' ),
    'description' => __( '', 'rizoma' ),
    'fields'      => array(
      array(
        'id'          => 'cycle_report',
        'label'       => __( 'Cycle', 'eitagcr' ),
        'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
        'type'        => 'select',
        'options'     => $cycles,
      ),
    ),
  );

  return $settings;
}

add_filter( 'eita_gcr_after_options', 'rizoma_load_report', 10, 2);
function rizoma_load_report( $html, $tab ){

  if( ! in_array( $tab, ['rizoma_report_1', 'rizoma_report_2', 'rizoma_report_3', 'rizoma_report_4', 'rizoma_report_5'] )) {
    return $html;
  }

  if( $tab == 'rizoma_report_1') {
    $cycle_id = get_option( 'eita_gcr_cycle_report' );
    [ $html_incomes_1, $html_incomes_2, $total_incomes ] = rizoma_incomes( $cycle_id );
    [ $html_expenses_1, $html_expenses_2, $total_expenses ] = rizoma_expenses( $cycle_id );
  }

  if( $tab == 'rizoma_report_2') {
    $cycle_id = get_option( 'eita_gcr_cycle_accounting_report' );
    [ $html_incomes_1, $html_incomes_2, $total_incomes ] = rizoma_incomes( $cycle_id );
    [ $html_expenses_1, $html_expenses_2, $total_expenses ] = rizoma_expenses( $cycle_id );
  }

  if( $tab == 'rizoma_report_1' || $tab == 'rizoma_report_2' ){
    if (($total_incomes - $total_expenses) > 0){
      $class = "blue";
    } else {
      $class = "red";
    }
    $result = "<p class='big $class'>Resultado operacional: " . wc_price($total_incomes - $total_expenses) . "</p>";

    $title = [];
    if( is_array( $cycle_id )){
      foreach ($cycle_id as $id) {
        $title[] = get_the_title( $id );
      }
      $title = implode( ", ", $title );
    } else {
      $title = get_the_title( $cycle_id );
    }

    $html .= "
      <div style='display: grid;grid-template-columns: 1fr 1fr;width: 800px;grid-gap: 40px;'>$html_incomes_1 $html_incomes_2 $html_expenses_1 $html_expenses_2 $result</div>
    ";
  }

  if( $tab == 'rizoma_report_3') {
    $args = array('post_type' => 'cycle', 'post_status' => 'publish', 'posts_per_page' => -1);
    $allcycles = get_posts( $args );

    $cycles = null;
    if ( count( $allcycles ) > 0 ) {
      foreach ($allcycles as $cycle) {
        $cycles[$cycle->ID] = $cycle->post_title;
      }
    } else {
      return;
    }

    $html .= "
      <table style='width: 700px' class='customer_supplier'>
        <thead>
          <tr>
            <th style='text-align: center; width: 80px; border: 1px solid black'>Ciclo</th>
            <th style='text-align: center; width: 80px; border: 1px solid black'>Resultado</th>
          </tr>
        </thead>
        <tbody>
    ";
    foreach ($cycles as $cycle_id => $cycle) {
      [ $html_incomes_1, $html_incomes_2, $total_incomes ] = rizoma_incomes( $cycle_id );
      [ $html_expenses_1, $html_expenses_2, $total_expenses ] = rizoma_expenses( $cycle_id );

      $class = "red";
      if( $total_incomes - $total_expenses > 0 ){
        $class = "blue";
      }
      $html .= "
        <tr class=$class>
          <th style='border: 1px solid black'>$cycle</th>
          <td style='border: 1px solid black'>" . wc_price( $total_incomes - $total_expenses ) . "</td>
        </tr>
      ";

    }
    $html .= "
      </tbody>
      </table>
    ";

  }

  if( $tab == 'rizoma_report_4') {
    $cycle_id = get_option( 'eita_gcr_cycle_report' );

    # get Orders
    $args = array(
      'post_type'      => 'shop_order',
      'posts_per_page' => -1,
      'post_status'    => ['wc-completed', 'wc-processing', 'wc-on-hold', 'wc-pending'],
      'meta_query'     => array(
        array(
          'key' => '_eita_gcr_cycle_id',
          'value' => $cycle_id,
        ),
        array(
          'key' => 'tipo_de_pedido',
          'value' => 'pedido_demandante_ofertante',
        ),
      ),
    );
    $orders = get_posts( $args );
    if( ! $orders ) { return $html; }

    $cycle_id = explode( ",",$cycle_id );
    $cycle_title = [];
    if( is_array( $cycle_id )){
      foreach ($cycle_id as $cid) {
        $cycle_title[] = get_the_title( $cid );
      }
      $cycle_title = implode(", ",$cycle_title);
    } else {
      $cycle_title = get_the_title( $cycle_id );
    }

    $html .= "
      <table style='width: 700px' class='customer_supplier'>
        <thead>
          <tr>
            <th colspan='4'>" . $cycle_title . "</th>
          </tr>
          <tr>
            <th style='border: 1px solid black; width: 50px; text-align: center;'>Demandante</th>
            <th style='border: 1px solid black; width: 50px; text-align: center;'>Ofertante</th>
            <th style='border: 1px solid black; width: 10px; text-align: center;'>Valor</th>
            <th style='border: 1px solid black; width: 10px; text-align: center;'>Comprovante</th>
          </tr>
        </thead>
        <tbody>
    ";

    foreach ( $orders as $order ) {
      $order_p = $order;
      $order = wc_get_order( $order );
      $demandante = $order->get_billing_first_name();
      $ofertante = wp_get_post_terms( $order_p->ID, 'supplier' );
      if( $ofertante ){
        $ofertante = $ofertante[0]->name;
      }
      $valor = wc_price( $order->get_total() );
      $data_de_pagamento = $order_p->data_de_pagamento;

      $comprovante = $order_p->comprovante_de_pagamento;
      if( $comprovante ){
        $comprovante = wp_get_attachment_link( $comprovante, 'thumbnail', false, true, get_the_title( $comprovante ) );
      } else {
        $comprovante = "";
      }

      $html .= "
        <tr>
          <td style='border: 1px solid black;'>$demandante</td>
          <td style='border: 1px solid black;'>$ofertante</td>
          <td style='border: 1px solid black;'>$valor</td>
          <td style='border: 1px solid black;'>$comprovante</td>
        </tr>
      ";
    }

    $html .= "
      </tbody>
      </table>
    ";

  }

  if( $tab == 'rizoma_report_5') {
    $cycle_id = get_option( 'eita_gcr_cycle_report' );

    # get Orders
    $args = array(
      'post_type'      => 'shop_order',
      'posts_per_page' => -1,
      'post_status'    => ['wc-completed', 'wc-processing', 'wc-on-hold', 'wc-pending'],
      'meta_query'     => array(
        array(
          'key' => '_eita_gcr_cycle_id',
          'value' => $cycle_id,
        ),
        array(
          'key' => 'tipo_de_pedido',
          'value' => 'pedido_ofertante',
        ),
      ),
    );
    $orders = get_posts( $args );
    if( ! $orders ) { return $html; }

    $html .= "
      <table style='width: 500px' class='customer_supplier'>
        <thead>
          <tr>
            <th colspan='5'>" . get_the_title( $cycle_id ) . "</th>
          </tr>
          <tr>
            <th style='width: 50px; border: 1px solid black;'>Demandante</th>
            <th style='width: 50px; border: 1px solid black;'>Ofertante</th>
            <th style='width: 10px; border: 1px solid black;'>Valor</th>
            <th style='width: 10px; border: 1px solid black;'>Nota Fiscal</th>
            <th style='width: 10px; border: 1px solid black;'>Comprovante de Pagamento</th>
          </tr>
        </thead>
        <tbody>
    ";

    foreach ( $orders as $order ) {
      $order_p = $order;
      $order = wc_get_order( $order );
      $demandante = $order->get_billing_first_name() . " " . $order->get_billing_last_name();
      $ofertante = wp_get_post_terms( $order_p->ID, 'supplier' );
      if( $ofertante ){
        $ofertante = $ofertante[0]->name;
      }
      $valor = wc_price( $order->get_total() );
      $data_de_pagamento = $order_p->data_de_pagamento;

      $comprovante = $order_p->comprovante_de_pagamento;
      if( $comprovante ){
        $comprovante = wp_get_attachment_link( $comprovante, 'thumbnail', false, true, get_the_title( $comprovante ) );
      } else {
        $comprovante = "";
      }

      $nota_fiscal = $order_p->nota_fiscal;
      if( $nota_fiscal ){
        $nota_fiscal = wp_get_attachment_link( $nota_fiscal, 'thumbnail', false, true, get_the_title( $nota_fiscal ) );
      } else {
        $nota_fiscal = "";
      }

      $html .= "
        <tr>
          <td style='border: 1px solid black;'>$demandante</td>
          <td style='border: 1px solid black;'>$ofertante</td>
          <td style='border: 1px solid black;'>$valor</td>
          <td style='border: 1px solid black;'>$nota_fiscal</td>
          <td style='border: 1px solid black;'>$comprovante</td>
        </tr>
      ";
    }

    $html .= "
      </tbody>
      </table>
    ";
  }

  if( is_array( $cycle_id )){
    $cycle_id = implode( ",", $cycle_id );
  }

  $html .= '
    <form id="download_report_xlsx" method="POST" action="options-general.php?page=eitagcr_settings">
      <input form="download_report_xlsx" type="hidden" name="download_report" value="yes">
      <input form="download_report_xlsx" type="hidden" name="report_format" value="XLSX">
      <input form="download_report_xlsx" type="hidden" name="cycle" value="' . $cycle_id . '">
      <input form="download_report_xlsx" type="hidden" name="report" value="' . $tab .'">
      <button class="button-secondary" form="download_report_xlsx" type="submit">XLSX</button>
    </form>
    <form id="download_report_pdf" method="POST" action="options-general.php?page=eitagcr_settings">
      <input form="download_report_pdf" type="hidden" name="download_report" value="yes">
      <input form="download_report_pdf" type="hidden" name="report_format" value="PDF">
      <input form="download_report_pdf" type="hidden" name="cycle" value="' . $cycle_id .'">
      <input form="download_report_pdf" type="hidden" name="report" value="' . $tab . '">
      <button class="button-secondary" form="download_report_pdf" type="submit">PDF</button>
    </form>
  ';

  return $html;
}


function rizoma_incomes( $cycle_id ){

  # get Orders
  $args = array(
    'post_type'      => 'shop_order',
    'posts_per_page' => -1,
    'post_status'    => ['wc-completed', 'wc-processing', 'wc-on-hold', 'wc-pending'],
    'meta_query'     => array(
      array(
        'key' => '_eita_gcr_cycle_id',
        'value' => $cycle_id,
      ),
      array(
        'key' => 'tipo_de_pedido',
        'value' => 'pedido_demandante_ofertante',
      ),
    ),
  );
  $orders = get_posts( $args );

  if( ! $orders ) {return "";}

  $total_items = [];
  $total_fees = [];
  $first_names = [];
  foreach ($orders as $order) {
    $order = wc_get_order( $order->ID );
    $user_id = $order->get_user()->ID;
    $first_names[ $user_id ] = $order->get_billing_first_name();
    if( isset( $total_items[$user_id] )){
      $total_items[ $user_id ] += $order->get_subtotal();
      $total_fees[ $user_id ] += $order->get_total_fees();
    } else {
      $total_items[ $user_id ] = $order->get_subtotal();
      $total_fees[ $user_id ] = $order->get_total_fees();
    }
  }

  $table_1 = "
    <table style='width: 400px'  class='customer_supplier'>
    <thead>
      <tr>
        <th  style='text-align: center; border: 1px solid black' colspan='2'>Receitas - Líquido</th>
      </tr>
      <tr>
        <th style='text-align: center; width: 40px; border: 1px solid black'>Demandante</th>
        <th style='text-align: center; width: 40px; border: 1px solid black'>Total</th>
      </tr>
    </thead>
    <tbody>
  ";

  foreach ($total_items as $user_id => $total_item) {
    $table_1 .= "
      <tr>
        <th style='border: 1px solid black' class='nome'>{$first_names[$user_id]}</th>
        <td style='border: 1px solid black' class='valor'>" . wc_price( $total_item ) . "</td>
      </tr>
    ";
  }
  $table_1 .= "
  <tr>
    <th style='border: 1px solid black' >Total</th>
    <th style='border: 1px solid black' class='valor'>" . wc_price( array_sum( $total_items )) . "</th>
  </tr>
    </tbody>
    </table>
  ";

  $table_2 = "
    <table class='customer_supplier'>
    <thead>
      <tr>
        <th style='text-align: center; border: 1px solid black' colspan='2'>Receitas - Margem Rizoma</th>
      </tr>
      <tr>
        <th style='text-align: center; width: 40px; border: 1px solid black'>Demandante</th>
        <th style='text-align: center; width: 40px; border: 1px solid black'>Total</th>
      </tr>
    </thead>
    <tbody>
  ";

  foreach ($total_fees as $user_id => $total_fee) {
    $table_2 .= "
      <tr>
        <th style='border: 1px solid black' class='nome'>{$first_names[$user_id]}</th>
        <td style='border: 1px solid black' class='valor'>" . wc_price($total_fee) . "</td>
      </tr>
    ";
  }
  $table_2 .= "
    <tr>
      <th style='border: 1px solid black'>Total</th>
      <th style='border: 1px solid black' class='valor'>" . wc_price( array_sum( $total_fees )) . "</th>
    </tr>
    </tbody>
    </table>
  ";

  return [ $table_1, $table_2, array_sum( $total_items ) + array_sum( $total_fees ) ];
}

function rizoma_expenses( $cycle_id ){

  $table_1 = "";
  $table_2 = "";

  # get Orders
  $args = array(
    'post_type'      => 'shop_order',
    'posts_per_page' => -1,
    'post_status'    => ['wc-completed', 'wc-processing', 'wc-on-hold', 'wc-pending'],
    'meta_query'     => array(
      array(
        'key' => '_eita_gcr_cycle_id',
        'value' => $cycle_id,
      ),
      array(
        'key' => 'tipo_de_pedido',
        'value' => 'pedido_ofertante',
      ),
    ),
  );
  $orders = get_posts( $args );

  if( ! $orders ) {return "";}

  $totals = [];
  foreach ($orders as $order) {
    $supplier = wp_get_post_terms( $order->ID, 'supplier' );
    $order = wc_get_order( $order->ID );
    if( $supplier ){
      $supplier = $supplier[0]->name;
    } else {
      continue;
    }
    if( isset($totals[ $supplier ]) ){
      $totals[ $supplier ] += $order->get_total();
    } else {
      $totals[ $supplier ] = $order->get_total();  
    }

  }

  $table_1 = "
    <table style='width: 400px' class='customer_supplier'>
    <thead>
      <tr>
        <th style='text-align: center; border: 1px solid black' colspan='2'>Despesas - Fornecedores</th>
      </tr>
      <tr>
        <th style='text-align: center; width: 40px; border: 1px solid black'>Ofertante</th>
        <th style='text-align: center; width: 40px; border: 1px solid black'>Total</th>
      </tr>
    </thead>
    <tbody>
  ";

  foreach ($totals as $supplier => $total) {
    $table_1 .= "
      <tr>
        <th style='border: 1px solid black' class='nome'>{$supplier}</th>
        <td style='border: 1px solid black' class='valor'>" . wc_price( $total ) . "</td>
      </tr>
    ";
  }
  $table_1 .= "
    <tr>
      <th style='border: 1px solid black'>Total</th>
      <th style='border: 1px solid black' class='valor'>" . wc_price( array_sum( $totals )) . "</th>
    </tr>
    </tbody>
    </table>
  ";

  # Extravio
  $extravio = get_user_by( 'login', 'extravio' );

  $args = array(
    'post_type'      => 'shop_order',
    'posts_per_page' => -1,
    'post_status'    => ['wc-completed', 'wc-processing', 'wc-on-hold', 'wc-pending'],
    'meta_query'     => array(
      array(
        'key' => '_customer_user',
        'value' => $extravio->ID,
      ),
      array(
        'key' => '_eita_gcr_cycle_id',
        'value' => $cycle_id,
      ),
    ),
  );
  $orders = get_posts( $args );

  $total_extravio = 0;
  if( $orders ){
    $total_extravio = 0;
    foreach ($orders as $order) {
      $order = wc_get_order( $order->ID );
      $total_extravio += $order->get_total();
    }

    $table_2 = "
      <table class='customer_supplier'>
      <thead>
        <tr>
          <th style='text-align: center; border: 1px solid black' colspan='2'>Despesas - Operacionais</th>
        </tr>
        <tr>
          <th style='text-align: center; width: 40px; border: 1px solid black'>Despesa</th>
          <th style='text-align: center; width: 40px; border: 1px solid black'>Total</th>
        </tr>
      </thead>
      <tbody>
    ";

    $table_2 .= "
      <tr>
        <th style='border: 1px solid black'>Extravio</th>
        <th style='border: 1px solid black'>" . wc_price( $total_extravio ) . "</th>
      </tr>
    ";

    $table_2 .= "
      </tbody>
      </table>
    ";
  }

  return [ $table_1, $table_2, array_sum( $totals ) + $total_extravio ];
}

add_filter( 'eitagcr_get_report', function( $html, $report ){ return rizoma_load_report( $html, $report );}, 10, 2 );
